function buildConfig(env) {
    console.log(`Env: ${env}`);  
    if(!env) {
        env= 'dev';
    }
    process.env.NODE_ENV = env;//JSON.stringify(env);
    return require('./config/' + env + '.js')({ env: env })
}

module.exports = buildConfig;