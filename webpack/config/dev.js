const webpackMerge = require('webpack-merge');
var webpack = require('webpack');
var path = require('path');

const commonConfig = require('./base.js');

module.exports = function (env) {
  return webpackMerge(commonConfig, {
    mode: "development",
    devtool: "source-map",
    devServer: {
      historyApiFallback: true,
      port: 8124
    }
  });
}