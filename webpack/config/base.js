var path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
var CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


var dest = '../../dist/'
const devMode = process.env.NODE_ENV !== 'prod';
console.log(`devMode ${devMode}`);

var config = {
    context: path.resolve(__dirname, "../../src"),
    entry: "./index.jsx",
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, dest),
        publicPath: "/"
    },
    //resolve: tell webpack the type of files to use in require
    resolve: {
        extensions: [".js", ".jsx", ".json", "scss", ".css"]
    },
    //setup loaders
    module: {
        rules: [{
            test: /\.(js|jsx)?$/,
            exclude: /node_modules/,
            loader: "babel-loader"
        }, {
            test: /\.scss$/,

            use: [
                {
                    loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
                },
                {
                    loader: "css-loader" // translates CSS into CommonJS
                },
                {
                    loader: "sass-loader" // compiles Sass to CSS
                }
            ],
            exclude: path.resolve(__dirname, "./node_modules/")
        },
        {
            test: /\.jpeg|gif|jpg|png$/,
            loader: "file-loader?name=[name].[ext]"
        },
        {
            test: /\.svg/,
            use: {
                loader: 'svg-url-loader',
                options: {
                    limit: 1
                }
            }
        }]
    },
    plugins: [
        new CaseSensitivePathsPlugin(),
        new MiniCssExtractPlugin({
            filename: "app.css",
            chunkFilename: "[id].css"
        }),
        new HtmlWebpackPlugin({
            inject: false,
            hash: true,
            template: './index.html',
            filename: 'index.html'
        })
    ]
}
module.exports = config;
