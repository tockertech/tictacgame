const webpackMerge = require('webpack-merge');
var webpack = require('webpack');
const commonConfig = require('./base.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');
var dest = '../../dist/'
var path = require("path");

module.exports = function (env) {
    return webpackMerge(commonConfig, {
        mode: 'production',
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('production')
                },
            }),
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            }),
            new CleanWebpackPlugin(path.resolve(__dirname, dest), {allowExternal: true})
        ]
    });
}
