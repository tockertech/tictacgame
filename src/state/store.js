import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import  {ticTacReducer  } from './reducers/reducers';


const logging = false;

const lgmw = ( { getState}) => (next) => (action) => {
    const state = getState();
    logging && console.log('Dispatching ' + action.type);
    const returnValue = next(action);
    logging && console.log('After dispatch: ' + JSON.stringify(returnValue));
    return returnValue;
}

const ticTacStore = applyMiddleware(thunk, lgmw)(createStore)(ticTacReducer);

export const getTicTacStore = () => {
    return ticTacStore;
}


