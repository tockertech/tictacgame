import * as reducers from '../reducers/reducers';
import * as operations from '../operations';
import { dispatch } from 'redux';

describe('Tc Tac Toe', () => {
    const newBoard = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ];

    const rowWinBaord = [
        [1, 2, 1],
        [1, 2, 1],
        [2, 2, 2]
    ];

    const colWinBaord = [
        [1, 2, 2],
        [1, 2, 1],
        [1, 1, 2],
    ];

    const drawBoard = [
        [1, 2, 1],
        [2, 1, 2],
        [2, 1, 2]
    ];

    const diagWinnerBoard = [
        [1, 2, 0],
        [2, 1, 2],
        [2, 1, 1]
    ]

    /************ Confirm Reset logic correct operation ************/
    it('Test initial state', () => {
        let initState = reducers.ticTacReducer(undefined, 'RESET');
        expect(initState).toEqual({
            board: [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]
            ],
            player: 1,
            scoreboard: {
                firstPlayer: 0,
                secondPlayer: 0,
                draws: 0
            }, 
            winner: null,
            gameover: false
        });
    });
    /************ Confirm winning logic correct operation ************/
    it('Test - New game state initialisation', () => {
        let state = reducers.ticTacReducer(undefined, 'NEW_GAME');
        expect(state.board).toEqual([
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ]);
        expect(state.player).toEqual(1);
        expect(state.winner).toBeNull;
        expect(state.gameover).toBeFalsy;
    });
    /************ Confirm row winning logic correct operation ************/
    it('Test - Confirm row winner logic', () => {
        let result = operations.isWinner(rowWinBaord, 2, 2, 1);    
        expect(result[0].lineType).toBe('row');
        expect(result[0].lineID).toBe(2);
        expect(result[0].from).toBe('block-20');
        expect(result[0].to).toBe(`block-2${rowWinBaord.length -1}`);

    });
    /************ Confirm col winning logic correct operation ************/
    it('Test - Confirm column winner logic', () => {
        let result = operations.isWinner(colWinBaord, 1, 0, 0);    
        expect(result[0].lineType).toBe('col');
        expect(result[0].lineID).toBe(0);
        expect(result[0].from).toBe('block-00');
        expect(result[0].to).toBe(`block-${colWinBaord.length -1 }0`);

    });
    /************ Confirm winning logic correct operation ************/
    it('Test - Confirm Diag winner logic', () => {
        let result = operations.isWinner(diagWinnerBoard, 1, 0, 0);    
        expect(result[0].lineType).toBe('diag');
        expect(result[0].IDs).toEqual(['00','11','22']);
    });

    /*
    Various other test still need to be done - test game draw, player turn
    */
});