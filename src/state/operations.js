
import { newGame, reset, playerMove, switchPlayer, winner, draw } from './actions/actions';
import { getTicTacStore } from '../state/store';

const playerTurn = (player, row, col) => (dispatch) => {

    dispatch(playerMove(player, row, col));

    let board = getTicTacStore().getState().board;

    let winningLine = isWinner(board, player, row, col);

    if (winningLine != null) {
        dispatch(winner(player, winningLine));
    } else if (isDraw(board)) {
        dispatch(draw());
    } else {
        dispatch(switchPlayer(player == 1 ? 2 : 1));
    }
}

const isWinner = (board, player, row, col) => {
    /**************************************************
     Get count of player in the played row and column to 
     check if it is a winning line. If a winning row the 
     row / col ID is returned.
     
     If a diagonal is a winning line, return the IDs of 
     the winning blocks.
     **************************************************/
    let winningLines = [];
    //Check row
    let countInRow = board[row].filter(square => square === player).length;
    if (countInRow == board[row].length) {
        winningLines.push(
            {
            from: "block-" + row + "0",
            to: "block-" + row + (board.length - 1),
            lineType: 'row',
            lineID: row
        }            
        );
    }
    //Check column
    let countInColumn = board.map(row => row[col]).filter(square => square === player).length;
    if (countInColumn == board[row].length) {
        winningLines.push({
            from: "block-0" + col,
            to: "block-" + (board.length - 1) + col,
            lineType: 'col',
            lineID: col
        });
    }

    /**************************************************
    This check for diagonals could be optimised
    if the size of the board had to increase.
     **************************************************/
    let blocksInRightDiagonal = [], blocksInLeftDiagonal = [];
    for (let i = 0; i < board.length; i++) {
        if (board[i][i] === player)
            blocksInRightDiagonal.push("" + i + i);
        if (board[i][board.length - 1 - i] == player)
            blocksInLeftDiagonal.push("" + i + (board.length - 1 - i));
    }

    if (blocksInRightDiagonal.length == board.length) {
        winningLines.push({
            from: "block-00",
            to: "block-" + (board.length - 1) + (board.length - 1),
            lineType: 'diag',
            IDs: blocksInRightDiagonal
            
        });
    }

    if (blocksInLeftDiagonal.length == board.length) {
        winningLines.push({
            to: "block-0" + (board.length - 1),
            from: "block-" + (board.length - 1) + "0",
            lineType: 'diag',
            IDs: blocksInLeftDiagonal
        });
    }
    return winningLines.length > 0 ? winningLines : null;

}

const isDraw = (board) => {
    /**************************************************
    Check to see if there are still any vacant blocks
        
    Alternative would be to have a counter in state (reset on game start)
    Each time a player moves => increment the counter. 
    If the counter = block count => draw
    **************************************************/
    return !board.some(row => row.some(col => col == 0));
}

export {
    reset,
    newGame,
    playerTurn,
    isWinner
};