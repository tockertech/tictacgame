
import * as actionTypes from './types';

//Action creator
export const createAction = (type, value) => {
    return {type, value};
}

//Bound Action Creators
export const newGame = () => createAction(actionTypes.NEW_GAME, null)
export const reset = () => createAction(actionTypes.RESET, null)
export const playerMove = (player, row, col) => createAction(actionTypes.PLAYER_MOVE, { player, row, col});
export const switchPlayer = (player) => createAction(actionTypes.SWITCH_PLAYER, player);
export const winner = (player, winningLine) => createAction(actionTypes.WINNER, {player, winningLine});
export const draw = () => createAction(actionTypes.DRAW, null);
