var types = {
    RESET: 'RESET',
    NEW_GAME : 'NEW_GAME',
    PLAYER_MOVE: 'PLAYER_MOVE',
    SWITCH_PLAYER: 'SWITCH_PLAYER',
    WINNER: 'WINNER',
    DRAW: 'DRAW'
}
module.exports = types;