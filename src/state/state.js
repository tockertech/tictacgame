import { cleanBoard, clearScoreboard } from './reducers/reducers';

export const state = {
    board: cleanBoard(),
    player: 1,
    gameover: false,
    winner: null,
    scoreboard: clearScoreboard()
};