import { combineReducers } from 'redux'
import * as actionTypes from '../actions/types';


export const cleanBoard = () => {
    return [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ];
}

export const clearScoreboard = () => {
    return {
        firstPlayer: 0,
        secondPlayer: 0,
        draws: 0
    };
}

const boardReducer = (state = cleanBoard(), action) => {
    switch(action.type) {
        case actionTypes.RESET:
        case actionTypes.NEW_GAME:
            return cleanBoard();
        case actionTypes.PLAYER_MOVE:
            let board = state.slice();
            board[action.value.row][action.value.col] = action.value.player;
            return board;
        default:
            return state;
    }
}

const playerReducer = (state = 1, action) => {
    switch(action.type) {
        case actionTypes.RESET:
        case actionTypes.NEW_GAME:
            return 1; //First player to start
        case actionTypes.SWITCH_PLAYER:
            return action.value; //Switch to another player
        default:
            return state;
    }
}

const gameOverReducer = (state = false, action) => {
    switch(action.type) {
        case actionTypes.RESET:
        case actionTypes.NEW_GAME:
            return false;
        case actionTypes.DRAW:
        case actionTypes.WINNER:
            return true;
        default:
            return state;
    }
}

const winnerReducer = (state = null, action) => {
    switch(action.type) {
        case actionTypes.RESET:
        case actionTypes.NEW_GAME:
            return null;
        case actionTypes.WINNER:
            return action.value;
        default:
            return state;

    }
}

const scoreboardReducer = (state = clearScoreboard(), action) => {
    let scoreboard = state;
    switch(action.type) {
        case actionTypes.RESET:
            return clearScoreboard();
        case actionTypes.WINNER:
            if(action.value.player == 1)
                scoreboard.firstPlayer++;
            else
                scoreboard.secondPlayer++;
            return scoreboard;
        case actionTypes.DRAW:
            scoreboard.draws++;
            return scoreboard;
        default:
            return state;
    }   
}

export const ticTacReducer = combineReducers({
    board: boardReducer,
    player: playerReducer,
    winner: winnerReducer,
    gameover: gameOverReducer,
    scoreboard: scoreboardReducer
});