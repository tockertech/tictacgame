import * as React from 'react';
import * as ReactDom from 'react-dom';
import { connect } from 'react-redux';
import { getTicTacStore } from '../state/store';

class Scoreboard extends React.Component {
    constructor(props) {
        super(props);
    }

    renderPlayerScore = (player) => {
        let score = player == 1 ? this.props.scoreboard.firstPlayer : this.props.scoreboard.secondPlayer;
        return <div className={"score player" + (this.props.player == player ? " current-player" : "")}>
            <div>Player {player}</div>
            <div>{score}</div>
        </div>;
    }

    render() {
        return <div className="row score-board">
            <div className="col">
                {this.renderPlayerScore(1)}
            </div>
            <div className="col score draws">
                <div>Draws</div>
                <div>{this.props.scoreboard.draws}</div>
            </div>
            <div className="col">
                {this.renderPlayerScore(2)}
            </div>
        </div>;
    }
}

const mapStateToProps = (state) => {
    return {
        winner: getTicTacStore().getState().winner,
        gameover: getTicTacStore().getState().gameover,
        scoreboard: getTicTacStore().getState().scoreboard,
        player: getTicTacStore().getState().player
    }
};

export default connect(mapStateToProps, null)(Scoreboard);