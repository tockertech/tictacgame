import * as React from 'react';
import * as Enzyme from 'enzyme';
import { Block } from '../block';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('<Block> component', function () {
    let defaultProps = {
        row: 0,
        col: 0,
        value: 0,
        onPlayerMove: jest.fn(),
        isWinningBlock: false,
        disabled: false
    };

    describe('Snapshots', () => {
        it('Should compare to snapshot = Player 2 in row 0 col 0 ', () => {
            let props = { ...defaultProps, value: 2 }
            let component = Enzyme.render(<Block {...props}/>);
            expect(component).toMatchSnapshot();
        });    
        it('Should compare to snapshot = isWinningBlock ', () => {
            let props = { ...defaultProps, isWinningBlock: true }
            let component = Enzyme.render(<Block {...props}/>);
            expect(component).toMatchSnapshot();
        });    
    }); 
    describe('Interactions', () => {
        let onClick;

        const mount = (props) => {
            const wrapper = Enzyme.mount(<Block {...props} />);
            return [wrapper, wrapper.instance()];
        };  

        afterEach(() => {
            onClick.mockClear();
        });      

        it("onClick should invoke onHandleClick", () => {
            const [wrapper, instance] = mount(defaultProps);
            onClick = jest.spyOn(defaultProps, 'onPlayerMove').mockImplementation(jest.fn());
            const items = wrapper.find('.board-block');
            expect(items.length).toBe(1);
            expect(onClick).not.toBeCalled(); 
            items.at(0).simulate('click');
            expect(onClick).toHaveBeenCalledTimes(1);           
        })
        it("onClick should not invoke onHandleClick as occupied block", () => {
            let props = { ...defaultProps, value: 1};
            const [wrapper, instance] = mount(props);
            onClick = jest.spyOn(props, 'onPlayerMove').mockImplementation(jest.fn());
            const items = wrapper.find('.board-block');
            expect(items.length).toBe(1);
            expect(onClick).not.toBeCalled(); 
            items.at(0).simulate('click');
            expect(onClick).toHaveBeenCalledTimes(0);           
        })
    })
    
});
    