import * as React from 'react';
import * as ReactDom from 'react-dom';
import PropTypes from 'prop-types';
export class Block extends React.Component {

    constructor(props) {
        super(props);
    }

    handleClick = () => {
        if (this.props.value == 0 && !this.props.disabled) {
            this.props.onPlayerMove(this.props.row, this.props.col);
        }
    }

    render() {
        let css = "board-block";
        /***************************************************** 
        Styling applied according to game status. Disable block 
        if game is either won, drawn or occupied. Add styling 
        if it is a winning block.
        ******************************************************/
        css += (this.props.disabled) ? " disabled" : "";
        if (this.props.isWinningBlock) {
            css += " winning-block";
        } else if (this.props.value > 0) {
            let value = (this.props.value == 1 ? "X" : "O");
            css += " player-" + this.props.value;
        }

        return <div className={css}
            onClick={this.handleClick}
            id={"block-" + this.props.row + this.props.col}>
            {this.props.value > 0 && (this.props.value == 1 ? "X" : "O")}
        </div>;
    }
}

Block.propTypes = {
    row: PropTypes.number.isRequired,
    col: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
    onPlayerMove: PropTypes.func.isRequired,
    isWinningBlock: PropTypes.bool,
    disabled: PropTypes.bool
}
