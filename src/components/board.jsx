import * as React from 'react';
import * as ReactDom from 'react-dom';
import { connect } from 'react-redux';
import { getTicTacStore } from '../state/store';
import * as game from '../state/operations';
import { Block } from './block';

class Board extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.reset();
    }

    handlePlayerMove = (row, col) => {
        this.props.playerMove(this.props.player, row, col)
    }

    isWinningBlock = (row, col) => {
        /*******************************************
        If there is a winner, check if the rendered
        block is in the winning line. 
        *******************************************/
        let inWinningLine = false;
        if (this.props.winner) {
            this.props.winner.winningLine.forEach(line => {
                switch (line.lineType) {
                    case 'row':
                        if (row == line.lineID)
                            inWinningLine = true;
                        break;
                    case 'col':
                        if (col == line.lineID)
                            inWinningLine = true;
                        break;
                    case 'diag':
                        if (line.IDs.indexOf("" + row + col) >= 0)
                            inWinningLine = true;
                        break;
                    default:
                        break;
                }
            });
            return inWinningLine;
        } else {
            return false;
        }
    }

    renderGameStatus() {
        if (!this.props.gameover)
            return null; //Game is not finished

        let status = this.props.winner ? `Player ${this.props.winner.player} Wins!` : "Game is a Draw!";

        return <div className="col">
            <div className="status">
                <div className="status-message">{status}</div>
                <div><input className="butt" type="button" value="Play again ..." onClick={this.props.newGame} /></div>
            </div>
        </div>;
    }

    render() {
        return [
            <div className="row actions" key="actions">
                <div className="col-sm-12 p-0">
                    <input className="butt reset" type="button" value="Reset" onClick={this.props.reset} />
                </div>
            </div>,
            <div className="row board" key="board">
                <div className="col-sm-12">
                    {this.props.board.map((row, i) => {
                        return <div className="row board-row" key={"row" + i}>
                            {row.map((block, j) => {
                                let isWinningBlock = this.isWinningBlock(i, j);
                                return <div className="col" key={"row" + i + "-" + j}>
                                    <Block row={i}
                                        col={j}
                                        value={block}
                                        onPlayerMove={this.handlePlayerMove}
                                        isWinningBlock={isWinningBlock}
                                        disabled={!!this.props.winner} />
                                </div>
                            })}
                        </div>
                    })}
                </div>
            </div>,
            <div className="row game_status" key="game_status">
                {this.renderGameStatus()}
            </div>
        ];
    }
}
const mapStateToProps = (state) => {
    return {
        board: getTicTacStore().getState().board,
        player: getTicTacStore().getState().player,
        winner: getTicTacStore().getState().winner,
        gameover: getTicTacStore().getState().gameover
    }
};

const mapDispatchToProps = (dispatch) => ({
    reset: () => dispatch(game.reset()),
    newGame: () => dispatch(game.newGame()),
    playerMove: (player, row, col) => dispatch(game.playerTurn(player, row, col))
})

export default connect(mapStateToProps, mapDispatchToProps)(Board);
