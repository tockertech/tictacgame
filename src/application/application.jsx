import * as React from 'react';
import * as ReactDom from 'react-dom';
import Board from '../components/board';
import Scoreboard from '../components/scoreboard';

export default class Application extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="container">
            <h1 className="heading">Tic-Tac-Toe</h1>
            <Board/>
            <Scoreboard/>
        </div>;
    }
}