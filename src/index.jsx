import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import { getTicTacStore } from './state/store';
import * as initialState from './state/state';
import Application from './application/application';
import './styles/site.scss';

const store = getTicTacStore(initialState.state);

ReactDom.render(
    <Provider store={store}>
        <Application />
    </Provider>,
    document.getElementById('app')
);
